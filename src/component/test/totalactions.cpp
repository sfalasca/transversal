#include "gtest/gtest.h"

#include "component.h"
#include "component/input.h"
#include "component/totalactions.h"

using namespace Component;

TEST(TotalActions, SmokeTest) {
  const auto inputs = std::vector<Input>{
      {"s1", "bbc", "a1"}, {"s2", "bbc", "a2"}, {"s3", "rai", "a3"},
      {"s1", "bbc", "a1"}, {"s3", "rai", "a2"}, {"s2", "bbc", "a3"},
      {"s4", "bbc", "a1"}, {"s2", "bbc", "a2"}, {"s1", "bbc", "a3"},
      {"s2", "bbc", "a1"}};

  auto ta = TotalActions{};
  for (const auto &input : inputs) {
    ta << input;
  }
  const auto &result = ta.get();
  ASSERT_NE(std::end(result), result.find("bbc"));
  EXPECT_EQ(8ul, result.find("bbc")->second);
  ASSERT_NE(std::end(result), result.find("rai"));
  EXPECT_EQ(2ul, result.find("rai")->second);
}

TEST(TotalActions, TestInput) {
  const auto inputs = readInput();

  auto ta = TotalActions{};
  for (const auto &input : inputs) {
    ta << input;
  }
  const auto &result = ta.get();
  ASSERT_NE(std::end(result), result.find("barclays"));
  EXPECT_EQ(87ul, result.find("barclays")->second);
  ASSERT_NE(std::end(result), result.find("bbc"));
  EXPECT_EQ(88ul, result.find("bbc")->second);
  ASSERT_NE(std::end(result), result.find("be"));
  EXPECT_EQ(78ul, result.find("be")->second);
  ASSERT_NE(std::end(result), result.find("nissan"));
  EXPECT_EQ(19ul, result.find("nissan")->second);
}
