#include "gtest/gtest.h"

#include "component.h"
#include "component/input.h"
#include "component/meansessionactions.h"

using namespace Component;

TEST(MeanSessionActions, SmokeTest) {
  const auto inputs = std::vector<Input>{
      {"s1", "bbc", "a1"}, {"s1", "bbc", "a2"}, {"s1", "bbc", "a3"},
      {"s2", "bbc", "a1"}, {"s2", "bbc", "a2"}, {"s2", "bbc", "a3"},
      {"s2", "bbc", "a4"}, {"s1", "abc", "a1"}, {"s2", "abc", "a1"},
      {"s2", "abc", "a2"}, {"s2", "abc", "a3"}, {"s1", "rai", "a1"},
      {"s1", "rai", "a2"}, {"s2", "rai", "a3"}, {"s2", "rai", "a1"},
      {"s3", "rai", "a2"}, {"s3", "rai", "a4"}};

  auto msa = MeanSessionActions{};
  for (const auto &input : inputs) {
    msa << input;
  }

  const auto &result = msa.get();
  ASSERT_NE(std::end(result), result.find("bbc"));
  EXPECT_DOUBLE_EQ(3.5, result.find("bbc")->second);
  ASSERT_NE(std::end(result), result.find("abc"));
  EXPECT_DOUBLE_EQ(2., result.find("abc")->second);
  ASSERT_NE(std::end(result), result.find("rai"));
  EXPECT_DOUBLE_EQ(2., result.find("rai")->second);
}

TEST(MeanSessionActions, TestInput) {
  const auto inputs = readInput();

  auto msa = MeanSessionActions{};
  for (const auto &input : inputs) {
    msa << input;
  }

  const auto &result = msa.get();
  ASSERT_NE(std::end(result), result.find("barclays"));
  EXPECT_DOUBLE_EQ(14.5, result.find("barclays")->second);
  ASSERT_NE(std::end(result), result.find("bbc"));
  EXPECT_LE(14.66, result.find("bbc")->second);
  EXPECT_GE(14.67, result.find("bbc")->second);
  ASSERT_NE(std::end(result), result.find("be"));
  EXPECT_DOUBLE_EQ(13., result.find("be")->second);
  ASSERT_NE(std::end(result), result.find("nissan"));
  EXPECT_DOUBLE_EQ(9.5, result.find("nissan")->second);
}
