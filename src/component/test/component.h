#pragma once

#include <vector>

namespace Component {
struct Input;
}

std::vector<Component::Input> readInput();
