#include "component.h"

#include "component/input.h"
#include <fstream>
#include <sstream>

std::vector<Component::Input> readInput() {
  std::vector<Component::Input> result;
  auto input = std::ifstream{"testdata/testinput.txt"};
  for (std::string line; std::getline(input, line);) {
    auto iss = std::istringstream{line};
    std::string session;
    std::string website;
    std::string action;
    iss >> session >> website >> action;
    result.emplace_back(Component::Input{session, website, action});
  }
  return result;
}
