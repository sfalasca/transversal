#include "gtest/gtest.h"

#include "component.h"
#include "component/input.h"
#include "component/topactions.h"

using namespace Component;

TEST(TopActions, SmokeTest) {
  const auto inputs = std::vector<Input>{
      {"s1", "bbc", "a1"}, {"s2", "bbc", "a2"}, {"s3", "rai", "a3"},
      {"s1", "bbc", "a1"}, {"s3", "rai", "a2"}, {"s2", "bbc", "a3"},
      {"s4", "bbc", "a1"}, {"s1", "bbc", "a3"}, {"s2", "bbc", "a1"}};

  auto ta = TopActions{};
  for (const auto &input : inputs) {
    ta << input;
  }
  for (auto i = 0ul; i < 3ul; ++i) {
    const auto &result = ta.get(i);
    EXPECT_EQ(i, result.size());
  }

  const auto &result = ta.get(2ul);
  ASSERT_EQ(2ul, result.size());
  EXPECT_EQ("a1", result[0].first);
  EXPECT_EQ(4ul, result[0].second);
  EXPECT_EQ("a3", result[1].first);
  EXPECT_EQ(3ul, result[1].second);
}

TEST(TopActions, PartialSortTest) {
  const auto inputs = std::vector<Input>{
      {"s1", "bbc", "a1"}, {"s1", "bbc", "a2"}, {"s1", "bbc", "a2"},
      {"s1", "bbc", "a3"}, {"s1", "bbc", "a3"}, {"s1", "bbc", "a3"},
      {"s1", "bbc", "a4"}, {"s1", "bbc", "a4"}, {"s1", "bbc", "a4"},
      {"s1", "bbc", "a4"}, {"s1", "bbc", "a5"}, {"s1", "bbc", "a5"},
      {"s1", "bbc", "a5"}, {"s1", "bbc", "a5"}, {"s1", "bbc", "a5"},
  };

  auto ta = TopActions{};
  for (const auto &input : inputs) {
    ta << input;
  }

  ASSERT_EQ(1ul, ta.get(1ul).size());
  EXPECT_EQ("a5", ta.get(1ul)[0].first);

  ASSERT_EQ(2ul, ta.get(2ul).size());
  EXPECT_EQ("a5", ta.get(2ul)[0].first);
  EXPECT_EQ("a4", ta.get(2ul)[1].first);

  ASSERT_EQ(3ul, ta.get(3ul).size());
  EXPECT_EQ("a5", ta.get(3ul)[0].first);
  EXPECT_EQ("a4", ta.get(3ul)[1].first);
  EXPECT_EQ("a3", ta.get(3ul)[2].first);

  ASSERT_EQ(4ul, ta.get(4ul).size());
  EXPECT_EQ("a5", ta.get(4ul)[0].first);
  EXPECT_EQ("a4", ta.get(4ul)[1].first);
  EXPECT_EQ("a3", ta.get(4ul)[2].first);
  EXPECT_EQ("a2", ta.get(4ul)[3].first);

  ASSERT_EQ(5ul, ta.get(5ul).size());
  EXPECT_EQ("a5", ta.get(5ul)[0].first);
  EXPECT_EQ("a4", ta.get(5ul)[1].first);
  EXPECT_EQ("a3", ta.get(5ul)[2].first);
  EXPECT_EQ("a2", ta.get(5ul)[3].first);
  EXPECT_EQ("a1", ta.get(5ul)[4].first);
}

TEST(TopActions, OutOfBound) {
  const auto inputs = std::vector<Input>{
      {"s1", "bbc", "a1"}, {"s2", "bbc", "a2"}, {"s3", "rai", "a3"},
      {"s1", "bbc", "a1"}, {"s3", "rai", "a2"}, {"s2", "bbc", "a3"},
      {"s4", "bbc", "a1"}, {"s1", "bbc", "a3"}, {"s2", "bbc", "a1"}};

  auto ta = TopActions{};
  for (const auto &input : inputs) {
    ta << input;
  }
  for (auto i = 0ul; i < 3ul; ++i) {
    const auto &result = ta.get(i);
    EXPECT_EQ(i, result.size());
  }

  const auto &result = ta.get(40ul);
  EXPECT_EQ(3ul, result.size());
}

TEST(TopActions, TestInput) {
  const auto inputs = readInput();

  auto ta = TopActions{};
  for (const auto &input : inputs) {
    ta << input;
  }

  const auto &result = ta.get(20ul);
  EXPECT_EQ(20ul, result.size());
  EXPECT_EQ("entrylist_00", result[0].first);
  EXPECT_EQ(38ul, result[0].second);
  EXPECT_EQ("entrylist_02", result[1].first);
  EXPECT_EQ(26ul, result[1].second);
  EXPECT_EQ("entry_11", result[2].first);
  EXPECT_EQ(24ul, result[2].second);
  EXPECT_EQ("entry_07", result[3].first);
  EXPECT_EQ(23ul, result[3].second);
  EXPECT_EQ("entrylist_01", result[4].first);
  EXPECT_EQ(22ul, result[4].second);
  EXPECT_EQ("entry_02", result[5].first);
  EXPECT_EQ(17ul, result[5].second);
  EXPECT_EQ("entry_10", result[6].first);
  EXPECT_EQ(16ul, result[6].second);
  EXPECT_EQ("entry_03", result[7].first);
  EXPECT_EQ(15ul, result[7].second);
  EXPECT_EQ("entry_09", result[8].first);
  EXPECT_EQ(12ul, result[8].second);
  EXPECT_EQ("entry_01", result[9].first);
  EXPECT_EQ(11ul, result[9].second);

  EXPECT_TRUE("entry_08" == result[10].first || "entry_13" == result[10].first);
  EXPECT_EQ(10ul, result[10].second);
  EXPECT_TRUE("entry_08" == result[11].first || "entry_13" == result[11].first);
  EXPECT_EQ(10ul, result[11].second);
  EXPECT_NE(result[10].first, result[11].first);

  EXPECT_TRUE("entry_04" == result[12].first || "entry_16" == result[12].first);
  EXPECT_EQ(9ul, result[12].second);
  EXPECT_TRUE("entry_04" == result[13].first || "entry_16" == result[13].first);
  EXPECT_EQ(9ul, result[13].second);
  EXPECT_NE(result[12].first, result[13].first);

  EXPECT_TRUE("entry_00" == result[14].first || "entry_14" == result[14].first);
  EXPECT_EQ(7ul, result[14].second);
  EXPECT_TRUE("entry_00" == result[15].first || "entry_14" == result[15].first);
  EXPECT_EQ(7ul, result[15].second);
  EXPECT_NE(result[14].first, result[15].first);

  EXPECT_EQ("redirect", result[16].first);
  EXPECT_EQ(6ul, result[16].second);
  EXPECT_EQ("entry_05", result[17].first);
  EXPECT_EQ(4ul, result[17].second);
  EXPECT_EQ("login", result[18].first);
  EXPECT_EQ(3ul, result[18].second);
  // EXPECT_EQ("entry_06", result[19].first); it's actuall entry_15 with my
  // ordering
  EXPECT_EQ(1ul, result[19].second);
}
