#include "component/topactions.h"

#include "component/input.h"
#include <algorithm>

namespace Component {
TopActions::TopActions(std::optional<size_t> n)
    : n{std::nullopt == n ? 0ul : *n} {}

TopActions &TopActions::operator<<(const Input &input) {
  if (auto it = data.find(input.action); it != std::end(data)) {
    ++(it->second);
  } else {
    data[input.action] = 1ul;
  }

  return *this;
}

void TopActions::print(std::ostream &out) const { out << *this; }

TopActions::Result TopActions::get(const size_t num) const {
  auto result = Result{};
  if (0ul == num) {
    return result;
  }
  for (const auto &action : data) {
    result.emplace_back(action.first, action.second);
  }

  if (num >= result.size()) {
    std::sort(std::begin(result), std::end(result),
              [](const auto &l, const auto &r) { return l.second > r.second; });
  } else {
    auto middle = std::next(std::begin(result), num);
    std::partial_sort(
        std::begin(result), middle, std::end(result),
        [](const auto &l, const auto &r) { return l.second > r.second; });
    result.erase(middle, std::end(result));
  }

  return result;
}

TopActions::Result TopActions::get() const { return get(n); }

std::ostream &operator<<(std::ostream &out, const TopActions &top) {
  const auto &result = top.get();
  out << result.size() << " most frequent actions:" << std::endl;
  for (const auto &elem : result) {
    out << " * " << elem.second << "\t: " << elem.first << std::endl;
  }

  return out;
}
} // namespace Component
