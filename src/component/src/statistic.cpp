#include "component/statistic.h"

namespace Component {
std::ostream &operator<<(std::ostream &out, const Statistic &obj) {
  obj.print(out);
  return out;
}
} // namespace Component
