#include "component/topactionsequences.h"

#include "component/input.h"
#include <algorithm>
#include <sstream>

namespace Component {
TopActionSequences::TopActionSequences(std::optional<size_t> n)
    : n{std::nullopt == n ? 0ul : *n} {}

TopActionSequences &TopActionSequences::operator<<(const Input &input) {
  if (std::nullopt == current) {
    std::stringstream ss;
    ss << '\'' << input.action << '\'';
    current = {input.website, input.sessionId, ss.str()};
  } else if (std::get<0>(*current) == input.website &&
             std::get<1>(*current) == input.sessionId) {
    auto &sequence = std::get<2>(*current);
    std::stringstream ss;
    ss << ", '" << input.action << '\'';
    sequence += ss.str();
  } else {
    data[std::get<0>(*current)] << Input{
        std::get<1>(*current), std::get<0>(*current), std::get<2>(*current)};
    std::stringstream ss;
    ss << '\'' << input.action << '\'';
    current = {input.website, input.sessionId, ss.str()};
  }

  return *this;
}

void TopActionSequences::print(std::ostream &out) const { out << *this; }

TopActionSequences::Result TopActionSequences::get(const size_t num) const {
  auto result = Result{};

  for (const auto &website : data) {
    result[website.first] = website.second.get(num);
  }

  return result;
}

TopActionSequences::Result TopActionSequences::get() const { return get(n); }

std::ostream &operator<<(std::ostream &out, const TopActionSequences &top) {
  const auto &result = top.get();
  out << result.size()
      << " most frequent action sequences per website:" << std::endl;

  for (const auto &website : result) {
    for (const auto &elem : website.second) {
      out << " * " << website.first << "\t- "
          << elem.second << "\t: " << elem.first << std::endl;
    }
  }
  return out;
}
} // namespace Component
