#include "component/totalactions.h"

#include "component/input.h"

namespace Component {
TotalActions &TotalActions::operator<<(const Input &in) {
  if (auto it = result.find(in.website); it != std::end(result)) {
    ++(it->second);
  } else {
    result[in.website] = 1ul;
  }
  return *this;
}

void TotalActions::print(std::ostream &out) const { out << *this; }

const TotalActions::Result &TotalActions::get() const { return result; }

std::ostream &operator<<(std::ostream &out, const TotalActions &total) {
  out << "Total number of actions per website:" << std::endl;
  const auto &result = total.get();
  for (const auto &elem : result) {
    out << " * " << elem.first << "\t: " << elem.second << std::endl;
  }
  return out;
}
} // namespace Component
