#include "component/meansessionactions.h"

#include "component/input.h"

namespace Component {
MeanSessionActions &MeanSessionActions::operator<<(const Input &input) {
  auto websiteIt = data.find(input.website);
  if (std::end(data) == websiteIt ||
      std::end(websiteIt->second) == websiteIt->second.find(input.sessionId)) {
    data[input.website][input.sessionId] = 1ul;
  } else {
    ++(data[input.website][input.sessionId]);
  }
  return *this;
}

void MeanSessionActions::print(std::ostream &out) const { out << *this; }

MeanSessionActions::Result MeanSessionActions::get() const {
  auto result = Result{};
  for (const auto &website : data) {
    auto n = 0ul;
    auto tot = 0ul;
    for (const auto &session : website.second) {
      tot += session.second;
      ++n;
    }
    result[website.first] = double(tot) / double(n);
  }
  return result;
}

std::ostream &operator<<(std::ostream &out, const MeanSessionActions &mean) {
  out << "Mean number of actions in a session per website:" << std::endl;
  const auto &result = mean.get();
  for (const auto &elem : result) {
    out << " * " << elem.first << "\t: " << elem.second << std::endl;
  }
  return out;
}
} // namespace Component
