#pragma once

#include "component/statistic.h"
#include <cstddef>
#include <iostream>
#include <string>
#include <unordered_map>

namespace Component {
struct Input;

struct MeanSessionActions : Statistic {
  using Result = std::unordered_map<std::string, double>;
  MeanSessionActions &operator<<(const Input &) override;
  void print(std::ostream &) const override;

  Result get() const;

private:
  std::unordered_map<std::string, std::unordered_map<std::string, size_t>> data;
};

std::ostream &operator<<(std::ostream &, const MeanSessionActions &);
} // namespace Component
