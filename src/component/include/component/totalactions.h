#pragma once

#include "component/statistic.h"
#include <cstddef>
#include <iostream>
#include <string>
#include <unordered_map>

namespace Component {
struct Input;

struct TotalActions : Statistic {
  using Result = std::unordered_map<std::string, size_t>;
  TotalActions &operator<<(const Input &) override;
  void print(std::ostream &) const override;

  const Result &get() const;

private:
  Result result;
};

std::ostream &operator<<(std::ostream &, const TotalActions &);
} // namespace Component
