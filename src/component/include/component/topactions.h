#pragma once

#include "component/statistic.h"
#include <cstddef>
#include <iostream>
#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

namespace Component {
struct Input;

struct TopActions : Statistic {
  using Result = std::vector<std::pair<std::string, size_t>>;
  TopActions(std::optional<size_t> n = std::nullopt);
  TopActions &operator<<(const Input &) override;
  void print(std::ostream &) const override;

  Result get(size_t num) const;
  Result get() const;

private:
  size_t n = 0;
  std::unordered_map<std::string, size_t> data;
};

std::ostream &operator<<(std::ostream &, const TopActions &);
} // namespace Component
