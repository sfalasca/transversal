#pragma once

#include <string>

namespace Component {
struct Input {
  std::string sessionId;
  std::string website;
  std::string action;
};
} // namespace Component
