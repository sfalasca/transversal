#pragma once

#include "component/statistic.h"
#include "component/topactions.h"
#include <cstddef>
#include <iostream>
#include <optional>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

namespace Component {
struct Input;

struct TopActionSequences : Statistic {
  // key represents the website, value is an ordered vector of
  // <action_sequences, num_occourrences>
  using Result =
      std::unordered_map<std::string,
                         std::vector<std::pair<std::string, size_t>>>;
  TopActionSequences(std::optional<size_t> n = std::nullopt);
  TopActionSequences &operator<<(const Input &) override;
  void print(std::ostream &) const override;

  Result get(size_t num) const;
  Result get() const;

private:
  size_t n = 0;
  // indexed by website
  std::unordered_map<std::string, TopActions> data;
  // website, session, action_sequence
  // action_sequence represented in the form "'action1', 'action2', ..."
  std::optional<std::tuple<std::string, std::string, std::string>> current;
};

std::ostream &operator<<(std::ostream &, const TopActionSequences &);
} // namespace Component
