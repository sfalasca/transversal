#pragma once

#include <iostream>

namespace Component {
struct Input;

struct Statistic {
  virtual Statistic &operator<<(const Input &) = 0;
  virtual void print(std::ostream &) const = 0;
  virtual ~Statistic() = default;
};

std::ostream &operator<<(std::ostream &, const Statistic &);
} // namespace Component
