#include "componentdriver.h"
#include "component/input.h"
#include "component/meansessionactions.h"
#include "component/statistic.h"
#include "component/topactions.h"
#include "component/totalactions.h"
#include <boost/iostreams/device/array.hpp>
#include <boost/iostreams/filter/newline.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/predef.h>
#include <memory>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

void execute(std::istream &istream, std::ostream &ostream, bool totalActions,
             bool meanSessionActions, size_t topActions) {
  using namespace Component;

  std::vector<std::unique_ptr<Component::Statistic>> statistics;
  if (totalActions) {
    statistics.emplace_back(std::make_unique<TotalActions>());
  }
  if (meanSessionActions) {
    statistics.emplace_back(std::make_unique<MeanSessionActions>());
  }
  if (0ul != topActions) {
    statistics.emplace_back(std::make_unique<TopActions>(topActions));
  }

  boost::iostreams::filtering_istream in;
#ifdef BOOST_OS_WINDOWS
  auto newline = boost::iostreams::newline::dos;
#elif defined BOOST_OS_MACOS
  auto newline = boost::iostreams::newline::mac;
#else
  auto newline = boost::iostreams::newline::posix;
#endif
  in.push(boost::iostreams::newline_filter(newline), 0);
  in.push(istream, 0);

  for (std::string line; std::getline(in, line);) {
    std::string session;
    std::string website;
    std::string action;
    std::istringstream iss{line};
    iss >> session >> website >> action;
    const auto input = Input{session, website, action};
    for (auto &statistic : statistics) {
      *statistic << input;
    }
  }

  for (const auto &statistic : statistics) {
    ostream << *statistic << std::endl;
  }
}
