#pragma once

#include <cstddef>
#include <iostream>

void execute(std::istream &istream, std::ostream &ostream, bool totalActions,
             bool meanSessionActions, size_t topActions);
