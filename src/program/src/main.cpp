#include "componentdriver.h"
#include <boost/program_options.hpp>
#include <iostream>

int main(int argc, char **argv) {
  try {
    auto desc = boost::program_options::options_description("Options");
    desc.add_options()("help,h", "help screen")(
        "totalactions", "total number of actions per website")(
        "meansessionactions",
        "mean number of actions in a session per website")(
        "topactions", boost::program_options::value<size_t>(),
        "\"arg\" most frequent actions and their counts");

    auto vm = boost::program_options::variables_map{};
    boost::program_options::store(
        boost::program_options::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help") || vm.count("h")) {
      std::cout << desc;
      return 0;
    }

    execute(std::cin, std::cout, vm.count("totalactions"),
            vm.count("meansessionactions"),
            vm.count("topactions") ? vm["topactions"].as<size_t>() : 0ul);
    return 0;
  } catch (const boost::program_options::error &e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
}
